# fs2-lucene - Lucene integration for fs2 [![Maven Central](https://maven-badges.herokuapp.com/maven-central/io.pig/fs2-lucene_2.12/badge.svg)](https://maven-badges.herokuapp.com/maven-central/io.pig/fs2-lucene_2.12) ![Code of Conduct](https://img.shields.io/badge/Code%20of%20Conduct-Scala-blue.svg)

## [Head on over to the microsite](https://valencik.github.io/fs2-lucene)

## Quick Start

To use fs2-lucene in an existing SBT project with Scala 2.12 or a later version, add the following dependencies to your
`build.sbt` depending on your needs:

```scala
libraryDependencies ++= Seq(
  "io.pig" %% "fs2-lucene" % "<version>"
)
```
