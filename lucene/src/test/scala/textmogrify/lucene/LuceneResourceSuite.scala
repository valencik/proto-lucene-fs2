package textmogrify
package lucene

import munit.CatsEffectSuite
import cats.effect._
import org.apache.lucene.analysis.en.EnglishAnalyzer

class AnalyzerResourceSuite extends CatsEffectSuite {

  test("tokenizer should work") {
    val analyzer = AnalyzerResource.tokenizer[IO](new EnglishAnalyzer())
    val actual = analyzer.use { f =>
      f("Hello my name is Neeko")
    }
    assertIO(actual, Vector("hello", "my", "name", "neeko"))
  }

  test("tokenizer should yield a func that can be used multiple times") {
    val analyzer = AnalyzerResource.tokenizer[IO](new EnglishAnalyzer())
    val actual = analyzer.use { f =>
      for {
        v1 <- f("Hello my name is Neeko")
        v2 <- f("I enjoy jumping on counters")
      } yield v1 ++ v2

    }
    assertIO(actual, Vector("hello", "my", "name", "neeko", "i", "enjoi", "jump", "counter"))
  }

  test("tokenizer should work with custom Analyzer") {
    import org.apache.lucene.analysis.Analyzer.TokenStreamComponents
    import org.apache.lucene.analysis.standard.StandardTokenizer
    import org.apache.lucene.analysis.en.PorterStemFilter
    import org.apache.lucene.analysis.LowerCaseFilter
    import org.apache.lucene.analysis.Analyzer

    val stemmer = AnalyzerResource.tokenizer[IO](new Analyzer {
      protected def createComponents(fieldName: String): TokenStreamComponents = {
        val source = new StandardTokenizer()
        val tokens = new LowerCaseFilter(source)
        new TokenStreamComponents(source, new PorterStemFilter(tokens))
      }
    })
    val actual = stemmer.use { f =>
      for {
        v1 <- f("Hello my name is Neeko")
        v2 <- f("I enjoy jumping on counters")
      } yield (v1 ++ v2).mkString(" ")
    }
    assertIO(actual, "hello my name is neeko i enjoi jump on counter")
  }

}
